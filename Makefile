# python-nmap Makefile

VERSION=`python setup.py -q version`
ARCHIVE=`python setup.py --fullname`

clean: clean-eggs clean-build
	@find . -iname '*.pyc' -delete
	@find . -iname '*.pyo' -delete
	@find . -iname '*~' -delete
	@find . -iname '*.swp' -delete
	@find . -iname '__pycache__' -delete

clean-eggs:
	@find . -name '*.egg' -print0|xargs -0 rm -rf --
	@rm -rf .eggs/

clean-build:
	@rm -fr build/
	@rm -fr dist/
	@rm -fr *.egg-info

manifest:
	@python setup.py sdist --manifest-only

tox:
	@tox

test:
	@(export NMAP_XML_VERSION=7.01 ; nosetests --with-coverage nmap -v)

qa:
	@flake8 nmap

coverage:
	@coverage html nmap/nmap.py

testcase:
	@./nmap-6.40/nmap -sV scanme.nmap.org -oX scanme_output-6.40.xml
	@./nmap-7.01/nmap -sV scanme.nmap.org -oX scanme_output-7.01.xml

install:
	@python3 setup.py install

archive: doc
	@python3 setup.py sdist
	@echo Archive is create and named dist/$(ARCHIVE).tar.gz
	@echo -n md5sum is :
	@md5sum dist/$(ARCHIVE).tar.gz

license:
	@python3 setup.py --license

register:
	@python3 setup.py register
	@python3 setup.py sdist upload

doc:
	@pydoc3 -w nmap/nmap.py
	@cd docs && make html


changelog:
	vi CHANGELOG

release: tox manifest doc changelog hgcommit register web2


.PHONY: web web2 test
